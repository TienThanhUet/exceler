package exceler

const (
	ExcelCol       = "excelCol"
	ExcelRow       = "excelRow"
	ExcelCellValue = "excelCellValue"
	DefaultSheet   = "Sheet1"
	NoTemplate     = ""
	DefaultFolder  = "./data"
)
