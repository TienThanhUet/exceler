package exceler

import (
	"fmt"
	"github.com/xuri/excelize/v2"
	"io"
)

type (
	ExcelReader struct{}
)

func NewExcelReader() *ExcelReader {
	return &ExcelReader{}
}

func (e *ExcelReader) ParseReader(p interface{}, r io.Reader, ps ParseStrategy, optionFuncs ...OptionFunc) ([]interface{}, error) {
	prototype, err := getStructPrototype(p)
	if err != nil {
		return nil, err
	}

	f, err := excelize.OpenReader(r)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := f.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	return ps(prototype, f, optionFuncs...)
}

func (e *ExcelReader) ParseFile(p interface{}, filePath string, ps ParseStrategy, optionFuncs ...OptionFunc) ([]interface{}, error) {
	prototype, err := getStructPrototype(p)
	if err != nil {
		return nil, err
	}

	f, err := excelize.OpenFile(filePath)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := f.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	return ps(prototype, f, optionFuncs...)
}
