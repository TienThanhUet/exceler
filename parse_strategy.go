package exceler

import (
	"fmt"
	"github.com/xuri/excelize/v2"
	"reflect"
	"strconv"
)

type (
	ParseStrategy func(model *Prototype, f *excelize.File, optionFuncs ...OptionFunc) ([]interface{}, error)
)

func (e *ExcelReader) ParserStrategyByRow(model *Prototype, f *excelize.File, optionFuncs ...OptionFunc) ([]interface{}, error) {
	option := getOption(optionFuncs...)
	rows, err := f.Rows(option.SheetName)
	if err != nil {
		return nil, err
	}

	result := make([]interface{}, 0)
	var curRow int
	for rows.Next() {
		columnValues, err := rows.Columns()
		if err != nil {
			fmt.Println(err)
		}

		if curRow < option.StartRow {
			curRow += 1
			continue
		}

		newItem := reflect.New(model.Type)
		for _, colExcel := range model.Fields {
			if colExcel.Col > len(columnValues) {
				return nil, fmt.Errorf("invalid column %v greater than size column is %v", colExcel.Col, len(columnValues))
			}

			colValue := columnValues[colExcel.Col]
			newItem, err = setField(newItem, colExcel, colValue)
			if err != nil {
				return nil, err
			}
		}
		result = append(result, newItem.Interface())
	}
	if err = rows.Close(); err != nil {
		return nil, err
	}
	return result, nil
}

func (e *ExcelReader) ParserStrategyByObject(model *Prototype, f *excelize.File, optionFuncs ...OptionFunc) ([]interface{}, error) {
	option := getOption(optionFuncs...)
	newItem := reflect.New(model.Type)
	for _, colExcel := range model.Fields {
		colName, err := excelize.ColumnNumberToName(colExcel.Col)
		if err != nil {
			return nil, err
		}

		colValue, err := f.GetCellValue(option.SheetName, colName+strconv.FormatInt(int64(colExcel.Row), 10))
		if err != nil {
			return nil, err
		}

		newItem, err = setField(newItem, colExcel, colValue)
		if err != nil {
			return nil, err
		}
	}

	return []interface{}{newItem.Interface()}, nil
}

func (e *ExcelReader) ParserStrategyByCell(model *Prototype, f *excelize.File, optionFuncs ...OptionFunc) ([]interface{}, error) {
	option := getOption(optionFuncs...)
	typeFieldMap := make(map[string]*Field, 0)
	for _, field := range model.Fields {
		if field.Row != 0 {
			typeFieldMap[ExcelRow] = field
		}
		if field.Col != 0 {
			typeFieldMap[ExcelCol] = field
		}
		if field.CellValue != "" {
			typeFieldMap[ExcelCellValue] = field
		}
	}

	rows, err := f.GetRows(option.SheetName)
	if err != nil {
		return nil, err
	}

	result := make([]interface{}, 0)
	for rowIdx, row := range rows {
		if rowIdx < option.StartRow {
			continue
		}
		for colIdx, value := range row {
			if colIdx < option.StartCol {
				continue
			}

			newItem := reflect.New(model.Type)
			newItem, err = setField(newItem, typeFieldMap[ExcelRow], rows[rowIdx][typeFieldMap[ExcelCol].Col])
			if err != nil {
				return nil, err
			}

			newItem, err = setField(newItem, typeFieldMap[ExcelCol], rows[typeFieldMap[ExcelRow].Row][colIdx])
			if err != nil {
				return nil, err
			}

			newItem, err = setField(newItem, typeFieldMap[ExcelCellValue], value)
			if err != nil {
				return nil, err
			}

			result = append(result, newItem.Interface())
		}
	}
	return result, nil
}
