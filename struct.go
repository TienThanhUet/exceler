package exceler

import (
	"github.com/xuri/excelize/v2"
	"reflect"
	"sync"
)

type (
	Prototype struct {
		Type   reflect.Type
		Fields []*Field
	}

	Field struct {
		Name      string
		Kind      reflect.Kind
		Col       int
		Row       int
		CellValue string
	}

	File struct {
		*excelize.File
		mu sync.Mutex
	}
)
