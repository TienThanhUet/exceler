package exceler

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

type (
	Device struct {
		Series string `excelCol:"1"`
		MID    string `excelCol:"2"`
		TID    string `excelCol:"3"`
	}

	Config struct {
		MerchantCode       string `excelCol:"A" excelRow:"2"`
		MerchantId         int    `excelCol:"B" excelRow:"2"`
		CurrentMaxMMC      int    `excelCol:"C" excelRow:"2"`
		SecretKeySpos      string `excelCol:"D" excelRow:"2"`
		SecretKeyQr        string `excelCol:"E" excelRow:"2"`
		UseNewMethodFormat bool   `excelCol:"F" excelRow:"2"`
	}

	Cell struct {
		Row   string `excelRow:"1"`
		Col   string `excelCol:"1"`
		Value string `excelCellValue:"x"`
	}
)

func TestExceler_ParseFile(t *testing.T) {
	excelize := NewExcelReader()
	data, err := excelize.ParseFile(&Device{}, "./device_spos_template.xlsx", excelize.ParserStrategyByRow, WithStartRow(6), WithSheetName(DefaultSheet))

	assert.NoError(t, err)
	assert.Equal(t, 1, len(data))
	exampleModel := data[0].(*Device)
	assert.Equal(t, "PE40209N61710", exampleModel.Series)
}

//func TestExceler_ParserStrategyByCell(t *testing.T) {
//	excelize := NewExcelReader()
//	data, err := excelize.ParseFile(&Cell{}, "./Nhat Tin Delivery_COD fee_20220615.xlsx", excelize.ParserStrategyByCell, WithStartRow(3), WithStartCol(4), WithSheetName("HN_Road"))
//
//	assert.NoError(t, err)
//	assert.NotNil(t, data)
//}
