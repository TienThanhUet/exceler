package exceler

import (
	"github.com/xuri/excelize/v2"
	"path/filepath"
	"reflect"
	"strconv"
	"sync"
)

type ExcelWriter struct {
	prototype *Prototype
	option    *Option
	filePath  string
	file      *File
}

func NewExcelWriter(p interface{}, filePath string, optionFuncs ...OptionFunc) (*ExcelWriter, error) {
	// Parse struct
	prototype, err := getStructPrototype(p)
	if err != nil {
		return nil, err
	}

	// FullFill file path
	filePath = fulfillFilePath(filePath)
	absFilepath, err := filepath.Abs(filePath)
	if err != nil {
		return nil, err
	}

	option := getOption(optionFuncs...)

	// Init file
	var file *excelize.File
	if option.Template != NoTemplate {
		if _, err := copyFile(option.Template, filePath); err != nil {
			return nil, err
		}
		file, err = excelize.OpenFile(filePath)
		if err != nil {
			return nil, err
		}
	} else {
		file = excelize.NewFile()
	}

	return &ExcelWriter{
		prototype: prototype,
		filePath:  absFilepath,
		option:    option,
		file: &File{
			mu:   sync.Mutex{},
			File: file,
		},
	}, nil
}

func (e *ExcelWriter) GetFilePath() string {
	return e.filePath
}

func (e *ExcelWriter) Write(arr []interface{}) error {
	e.file.mu.Lock()
	defer e.file.mu.Unlock()

	err := e.doWrite(arr)
	if err != nil {
		deleteFile(e.filePath)
		return err
	}
	return nil
}

func (e *ExcelWriter) doWrite(arr []interface{}) error {
	var style, _ = e.file.NewStyle(&excelize.Style{
		Border: []excelize.Border{
			{Type: "left", Color: "000000", Style: 1},
			{Type: "top", Color: "000000", Style: 1},
			{Type: "bottom", Color: "000000", Style: 1},
			{Type: "right", Color: "000000", Style: 1},
		},
	})

	for _, item := range arr {
		e.option.StartRow = e.option.StartRow + 1
		for _, field := range e.prototype.Fields {
			col, err := excelize.ColumnNumberToName(field.Col)
			if err != nil {
				return err
			}

			valueItem := reflect.ValueOf(item)
			valueField := valueItem.FieldByName(field.Name)
			axis := col + strconv.FormatInt(int64(e.option.StartRow), 10)

			err = e.file.SetCellValue(e.option.SheetName, axis, getValueField(valueField, field))
			if err != nil {
				return err
			}

			e.file.SetCellStyle(e.option.SheetName, axis, axis, style)
		}
	}

	if err := e.file.SaveAs(e.filePath); err != nil {
		return err
	}
	return nil
}
