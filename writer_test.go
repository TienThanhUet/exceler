package exceler

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestExceler_WriteFile(t *testing.T) {
	excelize, err := NewExcelWriter(&Device{}, "./data/1656051773742399816.xlsx", WithStartRow(7), WithTemplate("./device_spos_template.xlsx"))
	assert.NoError(t, err)
	err = excelize.Write([]interface{}{
		Device{
			Series: "1",
			MID:    "1",
			TID:    "1",
		},
		Device{
			Series: "2",
			MID:    "2",
			TID:    "2",
		},
	})

	assert.NoError(t, err)

	err = excelize.Write([]interface{}{
		Device{
			Series: "3",
			MID:    "3",
			TID:    "3",
		},
		Device{
			Series: "5",
			MID:    "5",
			TID:    "5",
		},
	})

	assert.NoError(t, err)
}
