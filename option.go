package exceler

type (
	Option struct {
		StartRow  int
		StartCol  int
		SheetName string
		Template  string
	}

	OptionFunc func(e *Option) *Option
)

func WithStartRow(startRow int) OptionFunc {
	return func(e *Option) *Option {
		startRow -= 1
		if startRow < 0 {
			startRow = 0
		}

		e.StartRow = startRow
		return e
	}
}

func WithStartCol(startCol int) OptionFunc {
	return func(e *Option) *Option {
		startCol -= 1
		if startCol < 0 {
			startCol = 0
		}

		e.StartCol = startCol
		return e
	}
}

func WithSheetName(name string) OptionFunc {
	return func(e *Option) *Option {
		e.SheetName = name
		return e
	}
}

func WithTemplate(template string) OptionFunc {
	return func(e *Option) *Option {
		e.Template = template
		return e
	}
}

func getOption(optionFuncs ...OptionFunc) *Option {
	option := &Option{
		SheetName: DefaultSheet,
	}

	for _, optionFunc := range optionFuncs {
		option = optionFunc(option)
	}

	return option
}
