package exceler

import (
	"errors"
	"fmt"
	"github.com/xuri/excelize/v2"
	"io"
	"log"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"strconv"
	"time"
)

func getStructPrototype(v interface{}) (*Prototype, error) {
	rfType := reflect.TypeOf(v)
	if rfType.Kind() == reflect.Ptr {
		rfType = rfType.Elem()
	}

	colExcels := make([]*Field, 0, rfType.NumField())
	for i := 0; i < rfType.NumField(); i++ {
		fieldI := rfType.Field(i)
		fieldITag := fieldI.Tag

		colNum, err := getValueTagInt(fieldITag, ExcelCol)
		if err != nil {
			return nil, err
		}

		rowNum, err := getValueTagInt(fieldITag, ExcelRow)
		if err != nil {
			return nil, err
		}

		cellValue := fieldITag.Get(ExcelCellValue)

		colExcels = append(colExcels, &Field{
			Kind:      fieldI.Type.Kind(),
			Col:       int(colNum),
			Row:       int(rowNum),
			CellValue: cellValue,
			Name:      fieldI.Name,
		})
	}
	return &Prototype{
		Type:   rfType,
		Fields: colExcels,
	}, nil
}

func getValueTagInt(fieldITag reflect.StructTag, tag string) (int64, error) {
	var value int64
	var err error
	excelTag := fieldITag.Get(tag)
	if excelTag != "" {
		value, err = strconv.ParseInt(excelTag, 10, 64)
		if err != nil {
			valueInt, err := excelize.ColumnNameToNumber(tag)
			if err != nil {
				return 0, err
			}
			value = int64(valueInt)
			return 0, err
		}
	}
	return value, nil
}

func setField(item reflect.Value, field *Field, value string) (reflect.Value, error) {
	switch field.Kind {
	case reflect.String:
		item.Elem().FieldByName(field.Name).SetString(value)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		colIntVal, err := strconv.ParseInt(value, 10, 64)
		if err != nil {
			return reflect.Value{}, fmt.Errorf("incorrect type field %v with value %v", field.Name, value)
		}
		item.Elem().FieldByName(field.Name).SetInt(colIntVal)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		colUIntVal, err := strconv.ParseUint(value, 10, 64)
		if err != nil {
			return reflect.Value{}, fmt.Errorf("incorrect type field %v with value %v", field.Name, value)
		}
		item.Elem().FieldByName(field.Name).SetUint(colUIntVal)
	case reflect.Float32, reflect.Float64:
		colFloatVal, err := strconv.ParseFloat(value, 64)
		if err != nil {
			return reflect.Value{}, fmt.Errorf("incorrect type field %v with value %v", field.Name, value)
		}
		item.Elem().FieldByName(field.Name).SetFloat(colFloatVal)
	case reflect.Bool:
		colBoolVal, err := strconv.ParseBool(value)
		if err != nil {
			return reflect.Value{}, fmt.Errorf("incorrect type field %v with value %v", field.Name, value)
		}
		item.Elem().FieldByName(field.Name).SetBool(colBoolVal)
	default:
		return reflect.Value{}, fmt.Errorf("unsupport type of field %v", field.Name)
	}
	return item, nil
}

func getValueField(item reflect.Value, field *Field) interface{} {
	switch field.Kind {
	case reflect.String:
		return item.String()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return item.Int()
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return item.Uint()
	case reflect.Float32, reflect.Float64:
		return item.Float()
	case reflect.Bool:
		return item.Bool()
	default:
		return ""
	}
}

func randFileName() string {
	now := time.Now().UnixNano()
	return fmt.Sprintf("/%d.xlsx", now)
}

func fulfillFilePath(filePath string) string {
	dir, file := filepath.Split(filePath)
	if path.Ext(file) == "." || path.Ext(file) == "" {
		dir = filePath
		file = randFileName()
	}

	if dir == "" {
		dir = DefaultFolder
	}

	if _, err := os.Stat(dir); errors.Is(err, os.ErrNotExist) {
		err := os.MkdirAll(dir, os.ModePerm)
		if err != nil {
			log.Println(err)
		}
	}

	return filepath.Join(dir, file)

}

func copyFile(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

func createFile(filePath string) error {
	_, err := os.Create(filePath)
	if err != nil {
		return err
	}
	return nil
}

func deleteFile(filePath string) {
	os.Remove(filePath)
}
